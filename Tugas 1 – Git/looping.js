var flag = 2;
console.log('Loooping PERTAMA');
while(flag <= 20) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(flag + ' - I love coding'); // Menampilkan nilai flag pada iterasi tertentu
  flag = flag + 1;
  flag++; // Me ngubah nilai flag dengan menambahkan 1
} 

var flag = 20;
console.log('Loooping KEDUA');
while(flag > 1) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(flag + ' - I will become a mobile developer'); // Menampilkan nilai flag pada iterasi tertentu
  flag = flag - 1;
  flag--; // Me ngubah nilai flag dengan menambahkan 1
} 

console.log('--------------SOAL 2-----------------\n');

for(var deret = 1; deret < 21; deret++) {
  if(deret % 2 == 0){
    console.log(deret + ' - Berkualitas');
  }
  else if(deret % 3 == 0 && deret %2 !=0) {
    console.log(deret + ' - I Love Coding ');
  }
  else {
    console.log(deret + ' - Santai');
  }
}


console.log("-----SOAL NO. 3------");

x=1;
y=1;
z="";

while(y<=4)
{
    while(x<=8)
    {
        z+="#";
        x++;
    }
    console.log(z);
    y++;
}

console.log("");
console.log("-----SOAL NO. 4------");

x=1;
y=1;
z="";

while(y<=7)
{
    while(x<=y)
    {
        z+="#";
        x++;
    }
    y++;
    x=1;
    console.log(z);
    z="";
}

console.log("");
console.log("-----SOAL NO. 5------");

x=1;
y=1;
c=1;
catur="";

while(y<=8)
{
    while (x<=8) 
    {
        if(c%2==0) {
          catur+="#";
        }
        else {
          catur+=" ";
        }
        x++;
        c++;
    }
    console.log(catur);
    x=1;
    c++;
    y++;
    catur="";
}