var nama = "";
var peran = "";
/* 
// Output untuk Input nama = '' dan peran = ''
"Nama harus diisi!"
 
//Output untuk Input nama = 'John' dan peran = ''
"Halo John, Pilih peranmu untuk memulai game!"
 
//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
"Selamat datang di Dunia Werewolf, Jane"
"Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"
 
//Output untuk Input nama = 'Jenita' dan peran 'Guard'
//"Selamat datang di Dunia Werewolf, Jenita"
//"Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf." */
 
function soalIfElse(nama,peran){
    if(nama == ''){
        console.log("Nama harus diisi!")
    } else if(nama && peran == ''){
        console.log('Halo ' + nama + ', Pilih peranmu untuk memulai game!')
    } else if(nama == 'jane' && peran == 'Penyihir'){
        console.log("Selamat datang di Dunia Werewolf, Jane\nHalo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
    }else if(nama == 'jenita' && peran == 'Guard'){
        console.log('Selamat datang di Dunia Werewolf, Jenita\nHalo Guard Jenita,kamu akan membantu melindungi temanmu dari serangan werewolf')
    } else if (nama == 'Junaedi' && peran == 'werewolf'){
      console.log('Selamat datang di Dunia Werewolf, Junaedi\nHalo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!')
    }
}

soalIfElse('','')
soalIfElse('John','')
soalIfElse('jane','Penyihir')
soalIfElse('jenita','Guard')
soalIfElse('Junaedi','werewolf')

/* TUGAS 2 CONDITIONAL SOAL B */
console.log("=========SOAL B===========");

var tanggal = 1;
var bulan = 5;
var tahun = 1945;


switch(bulan) {
    case 1 :  { bulan = "Januari";break;}
    case 2 :  { bulan = "Februari";break;}
    case 3 :  { bulan = "Maret";break;}
    case 4 :  { bulan = "April";break;}
    case 5 :  { bulan = "Mei";break;}
    case 6 :  { bulan = "Juni";break;}
    case 7 :  { bulan = "Juli";break;}
    case 8 :  { bulan = "Agustus";break;}
    case 9 :  { bulan = "September";break;}
    case 10 :  { bulan = "Oktober";break;}
    case 11 :  { bulan = "November";break;}
    case 12 :  { bulan = "Desember";break;}
    default : { console.log("Bulan tidak valid");}
}

if(tanggal <1 || tanggal > 31) {
    console.log("Tanggal tidak valid");
}
if(tahun < 1900 || tahun > 2200) {
    console.log("Tahun tidak valid");
}

    console.log(tanggal+" "+bulan+" "+tahun);

