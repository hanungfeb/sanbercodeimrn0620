console.log("-----SOAL NO. 1------")
var x = 2;
var txt = "I love coding";

console.log("LOOPING PERTAMA");
while(x <= 20)
{
    console.log(x + " - "+ txt)
    x+=2;
}

x-=2;
console.log("LOOPING KEDUA");
txt = "I will become a mobile developer";
while(x>=2)
{
    console.log(x + " - "+ txt)
    x-=2;
}

console.log("");
console.log("-----SOAL NO. 2------");

for(i=1;i<=20;i++)
{
    if(i%2 == 0) {
        console.log(i + " - "+ "Berkualitas");
    }
    else if(i%3==0 && i%2!=0) {
        console.log(i+" - "+"I Love Coding");
    }
    else {
        console.log(i+" - "+"Santai");
    }
}

console.log("");
console.log("-----SOAL NO. 3------");

x=1;
y=1;
z="";

while(y<=4)
{
    while(x<=8)
    {
        z+="#";
        x++;
    }
    console.log(z);
    y++;
}

console.log("");
console.log("-----SOAL NO. 4------");

x=1;
y=1;
z="";

while(y<=7)
{
    while(x<=y)
    {
        z+="#";
        x++;
    }
    y++;
    x=1;
    console.log(z);
    z="";
}

console.log("");
console.log("-----SOAL NO. 5------");

x=1;
y=1;
c=1;
papan="";

while(y<=8)
{
    while (x<=8) 
    {
        if(c%2==0) {
            papan+="#";
        }
        else {
            papan+=" ";
        }
        x++;
        c++;
    }
    console.log(papan);
    x=1;
    c++;
    y++;
    papan="";
}